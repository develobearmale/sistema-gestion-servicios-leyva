package com.gest.services.dao.util;

import java.sql.Connection;
import java.sql.DriverManager;

public class Conexion {
	public static Connection doConnect() throws Exception{
		Connection connection = null;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/db_gestservices?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC","root","qweret123");
			/*connection = DriverManager.getConnection("jdbc:sqlserver://localhost:1433;"+
					"databaseName=BD_Teoma_Evo", "sa", "123456");*/
		} catch (Exception e) {
			throw e;
		}
		return connection;
	}
}
