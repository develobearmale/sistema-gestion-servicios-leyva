package com.gest.services.dao.common;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import com.gest.services.dao.util.Conexion;
import com.gest.services.domain.pojo.Service;
import com.gest.services.domain.pojo.ServiceType;
import com.gest.services.domain.pojo.User;

public class daoService {
	
	public static daoService _Instancia;
	private daoService() {}
	public static daoService Instancia() {
		if(_Instancia==null) {
			_Instancia = new daoService();
		}
		return _Instancia;
	}
	
	public ArrayList<Service> GetServicesByType(String nameServiceType) throws Exception{
		Connection cn = Conexion.doConnect();
		ArrayList<Service> listServices = new ArrayList<Service>();
		try {
		    String query = "SELECT s.id as idService, s.name as nameService, s.price as priceService, st.id as idServiceType, st.name as nameServiceType FROM service as s inner join servicetype as st \r\n" + 
		    		"ON s.idServiceType = st.id WHERE st.name='"+nameServiceType+"' GROUP BY s.name;";
		    		
		    		/* "SELECT s.id as idService, s.name as nameService, s.price as priceService, st.id as idServiceType, st.name as nameServiceType FROM service as s inner join servicetype as st \r\n" + 
		    		"ON s.idServiceType = '"+nameServiceType+"' GROUP BY s.name;";*/
		    Statement st = cn.createStatement();
			ResultSet rs = st.executeQuery(query); 
			while(rs.next()){
				Service objService = new Service();
					objService.setId(rs.getInt("idService"));
					objService.setName(rs.getString("nameService"));
					objService.setPrice(rs.getDouble("priceService"));
					ServiceType objServiceType = new ServiceType();
						objServiceType.setId(rs.getInt("idServiceType"));
						objServiceType.setName(rs.getString("nameServiceType"));
					objService.setObjServiceType(objServiceType);
				listServices.add(objService);		
			}
		} catch (Exception e) { throw e;}
		finally{cn.close();}
		return listServices;
	}	


}
