package com.gest.services.dao.common;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import com.gest.services.dao.util.Conexion;
import com.gest.services.domain.pojo.Service;
import com.gest.services.domain.pojo.ServiceType;

public class daoServiceType {
	public static daoServiceType _Instancia;
	private daoServiceType() {}
	public static daoServiceType Instancia() {
		if(_Instancia==null) {
			_Instancia = new daoServiceType();
		}
		return _Instancia;
	}

	public ArrayList<String> GetServiceTypes() throws Exception{
		Connection cn = Conexion.doConnect();
		ArrayList<String> listServiceTypes = new ArrayList<String>();
		try {
		    String query = "SELECT * FROM servicetype;";
		    Statement st = cn.createStatement();
			ResultSet rs = st.executeQuery(query); 
			while(rs.next()){
				String nameServiceType = new String();
				nameServiceType = rs.getString("name");
				listServiceTypes.add(nameServiceType);		
			}
		} catch (Exception e) { throw e;}
		finally{cn.close();}
		return listServiceTypes;
	}
}
