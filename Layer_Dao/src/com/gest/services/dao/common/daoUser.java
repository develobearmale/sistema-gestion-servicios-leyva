package com.gest.services.dao.common;

import java.sql.Connection;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import com.gest.services.dao.util.Conexion;
import com.gest.services.domain.pojo.User;

public class daoUser {
	
	public static daoUser _Instancia;
	private daoUser() {};
	public static daoUser Instancia() {
		if(_Instancia==null) {
			_Instancia = new daoUser();
		}
		return _Instancia;
	}
	
	public User getUser(String Username, String Password) throws Exception{
		Connection cn = Conexion.doConnect();
		User objUser = null;
		try {
		    String query = "SELECT * FROM User as u where u.active = true and u.username='"+Username+"' and u.password = '"+Password+"'";
		    Statement st = cn.createStatement();
			ResultSet rs = st.executeQuery(query); 
			if(rs.next()){
				objUser = new User();	
					objUser.setId(rs.getInt("id"));
					objUser.setUsername(rs.getString("username"));
					objUser.setPassword(rs.getString("password"));
					objUser.setActive(rs.getBoolean("active"));
					objUser.setType(rs.getString("type"));
			}
		} catch (Exception e) { throw e;}
		finally{cn.close();}
		return objUser;
	}

}
