package com.gest.services.presentation.controllers;

import com.gest.services.domain.pojo.User;
import com.get.services.logic.common.logicUser;

public class LoginController {
	
	public static LoginController _Instancia;
	private LoginController() {};
	public static LoginController Instancia() {
		if(_Instancia==null) {
			_Instancia = new LoginController();
		}
		return _Instancia;
	}
	
	public Boolean checkUser(String Username, String Password){
		Boolean active = false;
		
		try {
			if(logicUser.Instancia().checkAccess(Username, Password).getActive()==true) {
				active = true;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("Nulo");
		}
		
		return active;
	}
	

}
