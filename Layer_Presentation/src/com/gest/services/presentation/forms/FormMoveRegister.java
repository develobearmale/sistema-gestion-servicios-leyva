package com.gest.services.presentation.forms;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Dialog.ModalityType;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JRadioButton;
import com.toedter.calendar.JDateChooser;

public class FormMoveRegister extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			FormMoveRegister dialog = new FormMoveRegister();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public FormMoveRegister() {
		setTitle("Registrar movimiento");
		setBounds(100, 100, 600, 350);
		setResizable(false);
		setLocationRelativeTo(null);
		setModalityType(ModalityType.APPLICATION_MODAL);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		
		JLabel label = new JLabel("Codmovimiento:");
		label.setHorizontalAlignment(SwingConstants.RIGHT);
		label.setBounds(30, 60, 120, 20);
		contentPanel.add(label);
		
		JLabel lblProducto = new JLabel("Producto:");
		lblProducto.setHorizontalAlignment(SwingConstants.RIGHT);
		lblProducto.setBounds(30, 90, 120, 20);
		contentPanel.add(lblProducto);
		
		JLabel lblPlaca = new JLabel("Placa:");
		lblPlaca.setHorizontalAlignment(SwingConstants.RIGHT);
		lblPlaca.setBounds(30, 120, 120, 20);
		contentPanel.add(lblPlaca);
		
		JLabel lblMovimiento = new JLabel("Movimiento:");
		lblMovimiento.setHorizontalAlignment(SwingConstants.RIGHT);
		lblMovimiento.setBounds(30, 150, 120, 20);
		contentPanel.add(lblMovimiento);
		
		JLabel lblMotivo = new JLabel("Motivo:");
		lblMotivo.setHorizontalAlignment(SwingConstants.RIGHT);
		lblMotivo.setBounds(30, 180, 120, 20);
		contentPanel.add(lblMotivo);
		
		JLabel lblObservaciones = new JLabel("Observaciones:");
		lblObservaciones.setHorizontalAlignment(SwingConstants.RIGHT);
		lblObservaciones.setBounds(30, 210,120, 20);
		contentPanel.add(lblObservaciones);
		
		JLabel lblNewLabel = new JLabel("MOVIMIENTO");
		lblNewLabel.setFont(new Font("SansSerif", Font.BOLD, 22));
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setBounds(12, 13, 570, 30);
		contentPanel.add(lblNewLabel);
		
		textField = new JTextField();
		textField.setBounds(178, 59, 176, 22);
		contentPanel.add(textField);
		textField.setColumns(10);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setBounds(177, 89, 177, 21);
		contentPanel.add(comboBox);
		
		textField_1 = new JTextField();
		textField_1.setBounds(178, 119, 176, 22);
		contentPanel.add(textField_1);
		textField_1.setColumns(10);
		
		JLabel lblCantidad = new JLabel("Cantidad:");
		lblCantidad.setBounds(372, 122, 56, 16);
		contentPanel.add(lblCantidad);
		
		textField_2 = new JTextField();
		textField_2.setBounds(440, 119, 95, 22);
		contentPanel.add(textField_2);
		textField_2.setColumns(10);
		
		JRadioButton rdbtnNewRadioButton = new JRadioButton("ENTRADA");
		rdbtnNewRadioButton.setHorizontalAlignment(SwingConstants.LEFT);
		rdbtnNewRadioButton.setBounds(178, 148, 93, 25);
		contentPanel.add(rdbtnNewRadioButton);
		
		JRadioButton rdbtnNewRadioButton_1 = new JRadioButton("SALIDA");
		rdbtnNewRadioButton_1.setHorizontalAlignment(SwingConstants.RIGHT);
		rdbtnNewRadioButton_1.setBounds(275, 148, 79, 25);
		contentPanel.add(rdbtnNewRadioButton_1);
		
		textField_3 = new JTextField();
		textField_3.setBounds(178, 179, 357, 22);
		contentPanel.add(textField_3);
		textField_3.setColumns(10);
		
		textField_4 = new JTextField();
		textField_4.setBounds(178, 209, 357, 22);
		contentPanel.add(textField_4);
		textField_4.setColumns(10);
		
		JDateChooser dateChooser = new JDateChooser();
		dateChooser.setBounds(440, 60, 95, 22);
		contentPanel.add(dateChooser);
		
		JLabel lblFecha = new JLabel("Fecha:");
		lblFecha.setHorizontalAlignment(SwingConstants.RIGHT);
		lblFecha.setBounds(372, 62, 56, 16);
		contentPanel.add(lblFecha);
		
		JButton btnRegistrar = new JButton("Registrar");
		btnRegistrar.setBounds(178, 265, 97, 25);
		contentPanel.add(btnRegistrar);
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.setBounds(372, 265, 97, 25);
		contentPanel.add(btnCancelar);
	}
}
