// @formatter:off
 package com.gest.services.presentation.forms;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JToolBar;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.Color;
import java.awt.SystemColor;
import javax.swing.SwingConstants;

public class FormMain extends JFrame {

	private JPanel contentPane;
	private static FormMain frame;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					frame = new FormMain();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public FormMain() throws IOException {
		setTitle("SISTEMA DE VENTAS Y SERVICIO TECNICO");
		
		ArrayList<JButton> listButton = new ArrayList<JButton>();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1200, 800);
		setResizable(false);
		setLocationRelativeTo(null);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JToolBar toolBar = new JToolBar();
		toolBar.addSeparator();
		toolBar.setFloatable(false);
		toolBar.setBounds(12, 13, 1170, 20);

		JButton btnCotizacion = new JButton("COTIZACION");
		JButton btnServicios = new JButton("SERVICIOS");
		JButton btnCaja = new JButton("CAJA");
		JButton btnMovimientos = new JButton("MOVIMIENTOS ALMACEN");
		JButton btnReportes = new JButton("REPORTES");
		JButton btnMantenimiento = new JButton("MANTENIMIENTO");

		listButton.add(btnCotizacion);
		listButton.add(btnServicios);
		listButton.add(btnCaja);
		listButton.add(btnMovimientos);
		listButton.add(btnReportes);
		listButton.add(btnMantenimiento);

		for (int i = 0; i < listButton.size(); i++) {
			int btnId = i;
			toolBar.add(listButton.get(i));
			listButton.get(i).addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {

					switch (listButton.get(btnId).getText()) {
					case "COTIZACION":
						JOptionPane.showMessageDialog(null, "COTI");
						break;
					case "SERVICIOS":
						FormServiceOrder formServiceOrder = new FormServiceOrder();
						formServiceOrder.setVisible(true);
						break;
					case "CAJA":
						JOptionPane.showMessageDialog(null, "CA");
						break;
					case "MOVIMIENTOS ALMACEN":
						FormMove formMove = new FormMove();
						formMove.setVisible(true);					
						break;
					case "REPORTES":
						JOptionPane.showMessageDialog(null, "REPO");
						break;
					case "MANTENIMIENTO":
						JOptionPane.showMessageDialog(null, "MANTI");
						break;
					}

				}
			});
		}
		
		JPanel panel = new JPanel();
		panel.setBackground(SystemColor.menu);
		panel.setBounds(820, 680, 300, 35);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblEmpleado = new JLabel("Empleado:");
		lblEmpleado.setHorizontalAlignment(SwingConstants.RIGHT);
		lblEmpleado.setBounds(12, 7, 100, 20);
		panel.add(lblEmpleado);
		
		JLabel lblNewLabel_1 = new JLabel("Elver Payrazaman");
		lblNewLabel_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_1.setBounds(120, 7, 150, 20);
		panel.add(lblNewLabel_1);

		contentPane.add(toolBar);

		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon(FormMain.class.getResource("/background_image.jpg")));
		lblNewLabel.setBounds(12, 44, 1170, 708);
		contentPane.add(lblNewLabel);

	}
}

