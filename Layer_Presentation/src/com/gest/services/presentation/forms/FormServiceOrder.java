package com.gest.services.presentation.forms;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import java.awt.Font;
import java.awt.Panel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.BoxLayout;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import com.toedter.calendar.JDateChooser;
import com.toedter.calendar.JMonthChooser;
import com.toedter.calendar.JDayChooser;
import com.toedter.components.JLocaleChooser;
import com.toedter.calendar.JYearChooser;
import com.toedter.components.JSpinField;
import com.gest.services.dao.common.daoService;
import com.gest.services.dao.common.daoServiceType;
import com.gest.services.domain.pojo.Service;
import com.gest.services.domain.pojo.ServiceType;
import com.mysql.cj.exceptions.CJCommunicationsException;
import com.toedter.calendar.JCalendar;
import java.awt.SystemColor;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListSelectionModel;
import javax.swing.JTable;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.DecimalFormat;

public class FormServiceOrder extends JDialog {
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTable table;
	private JTable table_1;
	private JTextField txtField_subTotal;
	private JTextField txtField_igv;
	private JTextField txtField_total;
	public String[] itemsCombobox;
	public DefaultTableModel model, model1;
	public DefaultTableCellRenderer renderer, renderer1;
	public JScrollPane scrollPanelServiceType, scrollPanelDetailService;
	public JComboBox comboBox;
	public ArrayList<Service> listServicesDetail;
	public DecimalFormat reduceValues;
	public boolean flagTableServices=false;
	public boolean flagTableDetail=false;

	


	public static void main(String[] args) {
		try {
			FormServiceOrder dialog = new FormServiceOrder();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public FormServiceOrder() {
		setTitle("Orden de Servicio");
		setBounds(100, 100, 720, 700);
		setResizable(false);
		setLocationRelativeTo(null);
		setModalityType(ModalityType.APPLICATION_MODAL);
		getContentPane().setLayout(null);
		{
			JLabel lblOrdenDeServicio = new JLabel("ORDEN DE SERVICIO");
			lblOrdenDeServicio.setFont(new Font("SansSerif", Font.BOLD, 22));
			lblOrdenDeServicio.setHorizontalAlignment(SwingConstants.CENTER);
			lblOrdenDeServicio.setBounds(12, 13, 690, 60);
			getContentPane().add(lblOrdenDeServicio);
		}
		
		listServicesDetail = new ArrayList<Service>();


		Panel panel = new Panel();
		panel.setBackground(SystemColor.menu);
		panel.setBounds(12, 79, 692, 200);
		getContentPane().add(panel);
		panel.setLayout(null);
		

		JLabel lblNewLabel = new JLabel("Codorden:");
		lblNewLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel.setBounds(54, 16, 74, 16);
		panel.add(lblNewLabel);

		JLabel lblCliente = new JLabel("Nombres cliente:");
		lblCliente.setHorizontalAlignment(SwingConstants.RIGHT);
		lblCliente.setBounds(12, 48, 116, 16);
		panel.add(lblCliente);

		JLabel lblNewLabel_1 = new JLabel("Empleado:");
		lblNewLabel_1.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel_1.setBounds(54, 118, 74, 16);
		panel.add(lblNewLabel_1);

		JLabel lblAuto = new JLabel("Auto:");
		lblAuto.setHorizontalAlignment(SwingConstants.RIGHT);
		lblAuto.setBounds(54, 153, 74, 16);
		panel.add(lblAuto);

		textField = new JTextField();
		textField.setEnabled(false);
		textField.setEditable(false);
		textField.setBounds(140, 13, 116, 22);
		panel.add(textField);
		textField.setColumns(10);

		textField_1 = new JTextField();
		textField_1.setBounds(140, 45, 181, 22);
		panel.add(textField_1);
		textField_1.setColumns(10);

		textField_2 = new JTextField();
		textField_2.setBounds(140, 115, 181, 22);
		panel.add(textField_2);
		textField_2.setColumns(10);

		textField_3 = new JTextField();
		textField_3.setBounds(140, 150, 116, 22);
		panel.add(textField_3);
		textField_3.setColumns(10);

		JLabel lblNewLabel_2 = new JLabel("Fecha Orden:");
		lblNewLabel_2.setBounds(335, 16, 83, 16);
		panel.add(lblNewLabel_2);

		JLabel lblApellidosCliente = new JLabel("Apellidos cliente:");
		lblApellidosCliente.setHorizontalAlignment(SwingConstants.RIGHT);
		lblApellidosCliente.setBounds(12, 83, 116, 16);
		panel.add(lblApellidosCliente);

		textField_4 = new JTextField();
		textField_4.setColumns(10);
		textField_4.setBounds(140, 80, 181, 22);
		panel.add(textField_4);

		JCalendar calendar = new JCalendar();
		calendar.setDecorationBackgroundColor(SystemColor.controlHighlight);
		calendar.setTodayButtonVisible(true);
		calendar.setDecorationBordersVisible(true);
		calendar.setBounds(430, 13, 232, 159);
		panel.add(calendar);

		Panel panel_1 = new Panel();
		panel_1.setBounds(12, 300, 692, 155);
		getContentPane().add(panel_1);
		panel_1.setLayout(null);

		JLabel lblNewLabel_3 = new JLabel("Tipo de servicio:");
		lblNewLabel_3.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel_3.setBounds(27, 13, 101, 16);
		panel_1.add(lblNewLabel_3);
			
		try {
			itemsCombobox =	 daoServiceType.Instancia().GetServiceTypes().toArray((new String[daoServiceType.Instancia().GetServiceTypes().size()]));
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		comboBox = new JComboBox();
		comboBox.addActionListener (new ActionListener () {
		    public void actionPerformed(ActionEvent e) {
		    	updateServices(table,model,comboBox.getSelectedItem().toString());//<System.out.println(comboBox.getSelectedItem().toString());
		    }
		});
				
		comboBox.setModel(new DefaultComboBoxModel(itemsCombobox));
		comboBox.setBounds(140, 12, 180, 19);
		panel_1.add(comboBox);
		
		JButton btnAdd = new JButton("Agregar");

		
		btnAdd.setBounds(506, 55, 97, 25);
		panel_1.add(btnAdd);

		JButton btnEliminar = new JButton("Eliminar");
		btnEliminar.setBounds(506, 93, 97, 25);
		panel_1.add(btnEliminar);

		JLabel lblDetallesDelServicio = new JLabel("Detalles del servicio");
		lblDetallesDelServicio.setFont(new Font("SansSerif", Font.BOLD, 15));
		lblDetallesDelServicio.setHorizontalAlignment(SwingConstants.CENTER);
		lblDetallesDelServicio.setBounds(22, 461, 402, 16);
		getContentPane().add(lblDetallesDelServicio);

		JLabel lblSubTo = new JLabel("SUB TOTAL:");
		lblSubTo.setHorizontalAlignment(SwingConstants.RIGHT);
		lblSubTo.setBounds(450, 498, 86, 16);
		getContentPane().add(lblSubTo);

		JLabel lblNewLabel_4 = new JLabel("IGV(18%):");
		lblNewLabel_4.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel_4.setBounds(450, 526, 86, 16);
		getContentPane().add(lblNewLabel_4);

		JLabel lblNewLabel_5 = new JLabel("TOTAL:");
		lblNewLabel_5.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel_5.setBounds(450, 555, 86, 16);
		getContentPane().add(lblNewLabel_5);

		txtField_subTotal = new JTextField();
		txtField_subTotal.setHorizontalAlignment(SwingConstants.RIGHT);
		txtField_subTotal.setEnabled(false);
		txtField_subTotal.setEditable(false);
		txtField_subTotal.setBounds(565, 495, 116, 22);
		getContentPane().add(txtField_subTotal);
		txtField_subTotal.setColumns(10);
		txtField_subTotal.setDisabledTextColor(Color.BLACK);

		txtField_igv = new JTextField();
		txtField_igv.setHorizontalAlignment(SwingConstants.RIGHT);
		txtField_igv.setEnabled(false);
		txtField_igv.setEditable(false);
		txtField_igv.setBounds(565, 523, 116, 22);
		getContentPane().add(txtField_igv);
		txtField_igv.setColumns(10);
		txtField_igv.setDisabledTextColor(Color.BLACK);

		txtField_total = new JTextField();
		txtField_total.setHorizontalAlignment(SwingConstants.RIGHT);
		txtField_total.setEnabled(false);
		txtField_total.setEditable(false);
		txtField_total.setBounds(565, 552, 116, 22);
		getContentPane().add(txtField_total);
		txtField_total.setColumns(10);
		txtField_total.setDisabledTextColor(Color.BLACK);


		JButton btnNewButton_1 = new JButton("GUARDAR");
		btnNewButton_1.setBounds(25, 627, 97, 25);
		getContentPane().add(btnNewButton_1);

		JButton btnNewButton_2 = new JButton("SALIR");
		btnNewButton_2.setBounds(327, 627, 97, 25);
		getContentPane().add(btnNewButton_2);

		JButton btnNewButton_3 = new JButton("LIMPIAR DATOS");
		btnNewButton_3.setBounds(158, 627, 133, 25);
		getContentPane().add(btnNewButton_3);

		String col[] = {"Servicio", "Precio" };
		String colDetailService[] = {"N�","Servicio", "Precio","Tipo de Servicio"};


		JScrollPane scrollPanelServiceType = new JScrollPane();
		scrollPanelServiceType.setBounds(12, 42, 399, 100);
		
		JScrollPane scrollPanelDetailService = new JScrollPane();
		scrollPanelDetailService.setBounds(25, 486, 399, 100);

		model = new DefaultTableModel(null, col);
		model1 = new DefaultTableModel(null, colDetailService);

		updateServices(table,model,"Lavado");
		
		table = new JTable(model);
		table_1 = new JTable(model1);
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				int row = table.getSelectedRow();
				String value = table.getModel().getValueAt(row, 0).toString();
				flagTableServices =true;
				//System.out.println(value);
			}
		});
		
		table_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				if(model1.getRowCount()>0) {
					int row = table.getSelectedRow();
					String value = table.getModel().getValueAt(row, 0).toString();
					flagTableDetail =true;
					System.out.println(value);
				}
			}
		});
		
		configurationTable(table,renderer,scrollPanelServiceType,panel_1);
		configurationTable(table_1, renderer1,scrollPanelDetailService,panel_1);
		getContentPane().add(scrollPanelDetailService);
		
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(flagTableServices) {
					updateDetailServices(table_1, model1, listServicesDetail,"Agregar");
					calculateValues(listServicesDetail);
					System.out.println(getName());
				}
				else {
					JOptionPane.showMessageDialog(null, "Seleccione un servicio para agregar");
				}

			}
		});
		btnEliminar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				/*
				if(flagTableDetail) {

				}
				else {
					JOptionPane.showMessageDialog(null, "Seleccione un servicio para agregar");
				}*/
				updateDetailServices(table_1, model1, listServicesDetail,"Eliminar");
				calculateValues(listServicesDetail);
				System.out.println(getName());
			}
		});
		reduceValues= new DecimalFormat("#.00");
		
	}
	
	public void calculateValues(ArrayList<Service> listServices) {
		double subTotal = 0, igv = 0, total = 0;
		for(Service objService : listServices) {
			subTotal = objService.getPrice() + subTotal;
		}
		igv = subTotal * 0.18;
		total = subTotal + igv;
		
		txtField_subTotal.setText(String.valueOf(reduceValues.format(subTotal))); 
		txtField_igv.setText(String.valueOf(reduceValues.format(igv))); 
		txtField_total.setText(String.valueOf(reduceValues.format(total))); 
	
	}
		
	public void updateServices(JTable objTable, DefaultTableModel objModel, String nameServiceType) {
		ArrayList<Service> listServices = new ArrayList<Service>();
		try {
			listServices= daoService.Instancia().GetServicesByType(nameServiceType);
			objModel.setRowCount(0);

			for (Service obj : listServices) {
				objModel.addRow(new Object[] {obj.getName(), obj.getPrice() // +usuario.getTipoUsuario(),
				});
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	public void updateDetailServices(JTable objTable, DefaultTableModel objModel,ArrayList<Service> list,String nameAction) {
		if(nameAction=="Agregar") {
			double servicePrice = Double.parseDouble(table.getModel().getValueAt(table.getSelectedRow(), 1).toString());
			Service objService = new Service();
				objService.setName(table.getModel().getValueAt(table.getSelectedRow(), 0).toString());
				objService.setPrice(servicePrice);
				objService.setId(0);
				ServiceType objServiceType = new ServiceType();
					objServiceType.setId(0);
					objServiceType.setName(comboBox.getSelectedItem().toString());
				objService.setObjServiceType(objServiceType);
				list.add(objService);
			objModel.setRowCount(0);
			
			for (int i = 0; i<list.size();i++) {
				objModel.addRow(new Object[] {i+1,list.get(i).getName(), list.get(i).getPrice(),list.get(i).getObjServiceType().getName() // +usuario.getTipoUsuario(),
				});
			}

		}
		else {
			objModel.removeRow(0);
			
		}
		


	}
	
	public void configurationTable(JTable objTable, DefaultTableCellRenderer objDefaultTable, JScrollPane objJScrollPane, Panel objPanel) {
		objTable.setDefaultEditor(Object.class, null);
		objDefaultTable = (DefaultTableCellRenderer)objTable.getDefaultRenderer(Object.class);
		objDefaultTable.setHorizontalAlignment( SwingConstants.CENTER );
		objTable.setFillsViewportHeight(true);
		objTable.setColumnSelectionAllowed(false);
		objTable.setRowSelectionAllowed(true);
		objJScrollPane.setViewportView(objTable);
		objPanel.add(objJScrollPane);
	}
}
