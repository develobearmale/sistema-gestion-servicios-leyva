package com.gest.services.domain.pojo;

public class DetailServiceAndOrder {
	private int id;
	private ServiceOrder objServiceOrder;
	private Service objService;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public ServiceOrder getObjServiceOrder() {
		return objServiceOrder;
	}
	public void setObjServiceOrder(ServiceOrder objServiceOrder) {
		this.objServiceOrder = objServiceOrder;
	}
	public Service getObjService() {
		return objService;
	}
	public void setObjService(Service objService) {
		this.objService = objService;
	}
}
