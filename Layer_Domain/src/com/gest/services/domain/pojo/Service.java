package com.gest.services.domain.pojo;

import java.util.ArrayList;

public class Service {
	private int id;
	private String name;
	private ServiceType objServiceType;
	private double price;
	public int getId() {
		return id;
	}    
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public ServiceType getObjServiceType() {
		return objServiceType;
	}
	public void setObjServiceType(ServiceType objServiceType) {
		this.objServiceType = objServiceType;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}

}
