package com.gest.services.domain.pojo;

import java.sql.Date;

public class Receipt {
	private int id;
	private double subTotal;
	private double IGV;
	private double total;
	private Date date;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public double getSubTotal() {
		return subTotal;
	}
	public void setSubTotal(double subTotal) {
		this.subTotal = subTotal;
	}
	public double getIGV() {
		return IGV;
	}
	public void setIGV(double iGV) {
		IGV = iGV;
	}
	public double getTotal() {
		return total;
	}
	public void setTotal(double total) {
		this.total = total;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
}
