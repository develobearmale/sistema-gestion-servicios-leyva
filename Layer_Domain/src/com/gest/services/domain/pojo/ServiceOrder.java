package com.gest.services.domain.pojo;

import java.sql.Date;
import java.util.ArrayList;

public class ServiceOrder {
	private int id;
	private int code;
	private Date date;
	private String clientName;
	private String clientLastName;
	private User objUser;
	private String licensePlate;
	private ArrayList<DetailServiceAndOrder> listDetailServiceAndOrder;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getClientName() {
		return clientName;
	}
	public void setClientName(String clientName) {
		this.clientName = clientName;
	}
	public String getClientLastName() {
		return clientLastName;
	}
	public void setClientLastName(String clientLastName) {
		this.clientLastName = clientLastName;
	}
	public User getObjUser() {
		return objUser;
	}
	public void setObjUser(User objUser) {
		this.objUser = objUser;
	}
	public String getLicensePlate() {
		return licensePlate;
	}
	public void setLicensePlate(String licensePlate) {
		this.licensePlate = licensePlate;
	}
	public ArrayList<DetailServiceAndOrder> getListDetailServiceAndOrder() {
		return listDetailServiceAndOrder;
	}
	public void setListDetailServiceAndOrder(ArrayList<DetailServiceAndOrder> listDetailServiceAndOrder) {
		this.listDetailServiceAndOrder = listDetailServiceAndOrder;
	}
}
